# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FeatureIterator
                                 A QGIS plugin
 Iterate over a vector layer s features
                             -------------------
        begin                : 2017-01-27
        copyright            : (C) 2017 by Centro de Informação Geoespacial do Exército
        email                : igeoe@igeoe.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load FeatureIterator class from file FeatureIterator.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .feature_iterator import FeatureIterator
    return FeatureIterator(iface)
