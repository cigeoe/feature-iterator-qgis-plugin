Feature Iterator is a plugin for QGis. Check it's homepage at http://plugins.qgis.org/plugins/FeatureIterator/

This plugin's purpose is to iterate over a vector layer's features, having the possibility to remove each feature during a step-by-step iteration. One can also define a pair attribute-value that will filter the features to be iterated. During iteration, viewport will be panned to where the feature is, using the same zoom level defined by user.

Usage and description:
- clicking on plugin button will open the corresponding widget:
![ALT](/images/image1.jpg)
- "layer to iterate" dropdown box: layer whose features we pretend to iterate over;
- "refresh" button: updates the layer set in "layer to iterate" dropdown. Need to be clicked whenever a layer is added or removed to the project;
- "iterate over features with attribute-value" radio box: choose this option to iterate over features that have the attribute and value defined in the appropriate dropdown boxes. The attribute dropdown has a special option, "all", to let one iterate over all features without any filter;
- "iterate over selected features" radio box: choose this option to iterate over a group of selected features. In this case, after activate this option, you then must use QGIS selection tool to select the features intended to be iterated. After selecting the features, you must press the "memorize features" button and after it, pressing "forward" or "backward" will iterate only over those features;
- "fit to feature bounds in spite of maintain current map zoom" checkbox: if selected, during the iteration QGIS will adjust the zoom level to fit the window to the feature bounds. When not active, the window will maintain the zoom level;
- "forward" button: iterates the features in ascending order (sorted by internal id);
- "backward" button: iterates the features in descending order (sorted by internal id);
- "remove" button: removes the current selected feature;
- "undo" button: undo the previous feature removals;
- example:
  - after adding layers to the project, the "layer to iterate" dropdown stills empty;
  ![ALT](/images/image2.jpg)
  - you should press the "refresh" button to update the available layers;
  ![ALT](/images/image3.jpg)
  - you can now choose the layer to iterate over its features;
  - the default "attribute" is "all". You can select a specific attribute and a specific value to filter the features;
  ![ALT](/images/image4.jpg)
  - to iterate, press "forward" or "backward" buttons. If the "fit to feature bounds" checkbox is active, the window will zoom to fit the feature bounds in it, otherwise the zoom level will be kept untouched;
  ![ALT](/images/image5.jpg)
  ![ALT](/images/image6.jpg)
  - pressing the "remove" button will delete the selected feature, advancing immediately to the next feature;
  - the "undo" button will undo the previous removal;
  ![ALT](/images/image7.jpg)

Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal